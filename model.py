import pymysql.cursors

class event:

    def __init__(self,id=0,name='',date='',htm=0):
        self.id = id
        self.name = name
        self.date = date
        self.htm = htm

    @staticmethod
    def koneksi():
        return pymysql.connect(host='localhost',user='root',password='',db='eo')
    
    def SetId(self,id):
        self.id = id
    
    def SetName(self,name):
        self.name = name

    def SetDate(self,date):
        self.date = date

    def SetHtm(self,htm):
        self.htm = htm

    @staticmethod
    def GetAll():
        conn = event.koneksi()
        cursor = conn.cursor()
        container = []
        sql = "select * from event"
        cursor.execute(sql)
        for id,name,date,htm in cursor.fetchall():
            model = event(id,name,date,htm)
            container.append(model)
        cursor.close()
        conn.close()
        return container

    @staticmethod
    def GetByID(id):
        conn = event.koneksi()
        cursor = conn.cursor()
        cursor.execute("select * from event where id=%s ",id)
        for id,name,date,htm in cursor.fetchall():
            model = event(id,name,date,htm)
        cursor.close()
        conn.close()
        return model

   
    def add(self):
        conn = event.koneksi()
        cursor = conn.cursor()
        cursor.execute("insert into event(name,date,htm)  VALUES ('{0}', '{1}', '{2}')" .format(self.name,self.date,self.htm))
        conn.commit()
        cursor.close()
        conn.close()

    def Update(self):
        conn = event.koneksi()
        cursor = conn.cursor()
        cursor.execute("Update event SET name='{0}',date='{1}',htm='{2}' WHERE id='{3}'" .format(self.name,self.date,self.htm,self.id))
        conn.commit()
        cursor.close()
        conn.close()
    
    
    def Delete(self):
        conn = event.koneksi()
        cursor = conn.cursor()
        cursor.execute("DELETE FROM event where id='{0}' ".format(self.id) )
        conn.commit()
        cursor.close()
        conn.close()



class User:

    
    @staticmethod
    def koneksi():
        return pymysql.connect(host='localhost',user='root',password='',db='eo')

    
    @staticmethod
    def Auth(username,password):
        conn = event.koneksi()
        cursor = conn.cursor()
        cursor.execute("select * from user where username='{0}' AND password='{1}' ".format(username,password))
        validasi = cursor.fetchone()
        cursor.close()
        conn.close()
        return validasi

        