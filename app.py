from flask import Flask,render_template,request,redirect,url_for,session
from model import *

app = Flask(__name__)

app.config['SECRET_KEY'] = 'stmikwiduri'

@app.route('/')
def index():
    if 'username' in session:
        return render_template('index.html', data = event.GetAll())       
    else:  
        return redirect('/Login')      
        

@app.route('/add',methods=['GET','POST'])
def add():
    if 'username' in session:
        if request.method == 'POST':
            name = request.form['name']
            date = request.form['date']
            htm = request.form['htm']
            id = 0
            model = event()
            model.SetName(name)
            model.SetDate(date)
            model.SetHtm(htm)
            model.add()
            return redirect('/')
        else:
            return render_template('add.html')
    else:
        return redirect('/Login')
        

@app.route('/update/<int:id>',methods=['GET','POST'])
def Update(id):
    if 'username' in session:
        if request.method == 'POST':
            id = request.form['id']
            name = request.form['name']
            date = request.form['date']
            htm = request.form['htm']
            model = event()
            model.SetId(id)
            model.SetName(name)
            model.SetDate(date)
            model.SetHtm(htm)
            model.Update()
            return redirect('/')
        else:
            model = event.GetByID(id)
            return render_template('update.html', model = model)       
    else:
         return redirect('/Login')
        

@app.route('/delete/<int:id>')
def delete(id):
    if 'username' in session:
        model = event()
        model.SetId(id)
        model.Delete()
        return redirect('/')
    else:        
        return redirect('/Login')
        

@app.route('/Login',methods=['GET','POST'])
def Login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        data = User.Auth(username,password)
        if data is None:
            error = 'Please retry again because your username or password incorrect'
            return render_template('Login.html',error = error)
        else:            
            session['username'] = username
            return redirect('/')
            
    else:
        return render_template('Login.html')

@app.route('/Logout')
def Logout():
    session.pop('username',None)
    return redirect('/Login')





if __name__ == '__main__':
    app.run(debug=True)